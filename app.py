import argparse
import builder as bd

parser = argparse.ArgumentParser(description='SD Card Boot files builder')

parser.add_argument('-HDL', action="store", default="hdl_2018_r1",dest='hdl')
parser.add_argument('-LINUX', action="store", default="2018_R1",dest='linux')
parser.add_argument('-VIVADO', action="store", default="2017.4",dest='vivado')

results = parser.parse_args()

#.VIVADO = '2018.2'
#.HDLBRANCH = 'master'
#.LINUXBRANCH = 'adrv9009_zc706'

print(results)

bb = bd.BootBuilder()

bb.VIVADO = results.vivado
bb.HDLBRANCH = results.hdl
bb.LINUXBRANCH = results.linux

bb.build_complete_pipeline_flood()


