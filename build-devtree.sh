#!/bin/bash

# Set variables
OUTPUT_TARGET=$1
VIVADO=$2
ARCH=$3
CROSS_COMPILE=$4
DEVICETREE=$5

# Config
source /opt/Xilinx/Vivado/$VIVADO/settings64.sh
export ARCH=$ARCH
export CROSS_COMPILE=$CROSS_COMPILE

# Get ref
cp -r linux_ref "linux_$ARCH"
# Build linux
cd "linux_$ARCH"
if [ "$ARCH" = "arm" ]; then
    make "$DEVICETREE.dtb"
else
    make "xilinx/$DEVICETREE.dtb"
fi
cd ..
if [ "$ARCH" = "arm" ]; then
    cp "linux_$ARCH/arch/$ARCH/boot/dts/$DEVICETREE.dtb"  "$OUTPUT_TARGET/devicetree.dtb"
else
    cp "linux_$ARCH/arch/$ARCH/boot/dts/xilinx/$DEVICETREE.dtb"  "$OUTPUT_TARGET/devicetree.dtb"
fi

# Check
echo "Checking Devicetree build output"
if [ ! -f $OUTPUT_TARGET/devicetree.dtb ]; then
	echo "Devicetree build failed"
	exit 1
fi

# Cleanup
#echo "Cleaning up"
#rm -rf "linux_$DEVICETREE"
exit 0
